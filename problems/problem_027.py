# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if len(values) == 0:
        return None

    for i in values:
        maximum = max(values)
    return maximum

values = [1,2,3,4]

print(max_in_list(values))
